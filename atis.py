# coding=utf-8
# Copyright 2020 HuggingFace Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lint as: python3
"""Introduction to the CoNLL-2003 Shared Task: Language-Independent Named Entity Recognition"""

import datasets


logger = datasets.logging.get_logger(__name__)


_CITATION = """\
@inproceedings{tjong-kim-sang-de-meulder-2003-introduction,
    title = "Introduction to the {C}o{NLL}-2003 Shared Task: Language-Independent Named Entity Recognition",
    author = "Tjong Kim Sang, Erik F.  and
      De Meulder, Fien",
    booktitle = "Proceedings of the Seventh Conference on Natural Language Learning at {HLT}-{NAACL} 2003",
    year = "2003",
    url = "https://www.aclweb.org/anthology/W03-0419",
    pages = "142--147",
}
"""

_DESCRIPTION = """\
The shared task of CoNLL-2003 concerns language-independent named entity recognition. We will concentrate on
four types of named entities: persons, locations, organizations and names of miscellaneous entities that do
not belong to the previous three groups.

The CoNLL-2003 shared task data files contain four columns separated by a single space. Each word has been put on
a separate line and there is an empty line after each sentence. The first item on each line is a word, the second
a part-of-speech (POS) tag, the third a syntactic chunk tag and the fourth the named entity tag. The chunk tags
and the named entity tags have the format I-TYPE which means that the word is inside a phrase of type TYPE. Only
if two phrases of the same type immediately follow each other, the first word of the second phrase will have tag
B-TYPE to show that it starts a new phrase. A word with tag O is not part of a phrase. Note the dataset uses IOB2
tagging scheme, whereas the original dataset uses IOB1.

For more details see https://www.clips.uantwerpen.be/conll2003/ner/ and https://www.aclweb.org/anthology/W03-0419
"""

_URL = None
_TRAINING_FILE = "train.txt"
_DEV_FILE = "dev.txt"
_TEST_FILE = "test.txt"


class MediaConfig(datasets.BuilderConfig):
    """BuilderConfig for MEDIA"""

    def __init__(self, **kwargs):
        """BuilderConfig for MEDIA.

        Args:
          **kwargs: keyword arguments forwarded to super.
        """
        super(MediaConfig, self).__init__(**kwargs)


class Media(datasets.GeneratorBasedBuilder):
    """Conll2003 dataset."""

    BUILDER_CONFIGS = [
        MediaConfig(name="media", version=datasets.Version("0.0.0"), description="Media dataset"),
    ]

    def _info(self):
        return datasets.DatasetInfo(
            description=_DESCRIPTION,
            features=datasets.Features(
                {
                    "id": datasets.Value("string"),
                    "tokens": datasets.Sequence(datasets.Value("string")),
                    "chunk_tags": datasets.Sequence(
                        datasets.features.ClassLabel(
                            names=['B-flight_stop',
 'B-flight_days',
 'I-compartment',
 'B-depart_time.end_time',
 'B-return_date.date_relative',
 'B-meal_description',
 'B-state_name',
 'I-meal_code',
 'B-depart_date.date_relative',
 'B-airline_name',
 'B-return_time.period_mod',
 'B-connect',
 'I-city_name',
 'B-stoploc.state_code',
 'I-period_of_day',
 'B-arrive_time.time',
 'I-round_trip',
 'B-arrive_time.end_time',
 'B-stoploc.airport_code',
 'I-flight_mod',
 'B-day_number',
 'I-or',
 'I-arrive_time.time',
 'I-flight_stop',
 'B-airport_name',
 'B-depart_time.time_relative',
 'I-depart_date.day_number',
 'I-fare_basis_code',
 'B-toloc.airport_name',
 'I-fromloc.city_name',
 'I-return_date.today_relative',
 'B-fromloc.state_code',
 'B-day_name',
 'B-arrive_date.today_relative',
 'B-depart_time.period_mod',
 'B-transport_type',
 'I-connect',
 'I-toloc.state_name',
 'I-transport_type',
 'I-toloc.city_name',
 'B-fromloc.state_name',
 'B-fare_amount',
 'B-toloc.airport_code',
 'I-airline_name',
 'B-economy',
 'B-toloc.country_name',
 'B-return_date.day_number',
 'I-depart_time.period_of_day',
 'I-return_date.date_relative',
 'B-depart_date.day_name',
 'B-arrive_date.day_name',
 'B-today_relative',
 'I-cost_relative',
 'I-airport_name',
 'B-fare_basis_code',
 'B-depart_time.period_of_day',
 'B-cost_relative',
 'B-arrive_date.day_number',
 'I-arrive_time.start_time',
 'B-return_date.month_name',
 'I-toloc.airport_name',
 'B-toloc.state_code',
 'I-restriction_code',
 'I-fare_amount',
 'I-arrive_time.end_time',
 'B-arrive_time.start_time',
 'B-depart_date.year',
 'I-flight_time',
 'B-return_time.period_of_day',
 'B-days_code',
 'B-depart_time.time',
 'B-return_date.day_name',
 'B-time_relative',
 'B-depart_date.month_name',
 'B-fromloc.airport_name',
 'I-arrive_time.period_of_day',
 'B-meal_code',
 'I-economy',
 'B-stoploc.city_name',
 'B-airline_code',
 'I-depart_time.start_time',
 'I-depart_time.end_time',
 'I-depart_date.day_name',
 'I-depart_time.time',
 'B-flight',
 'B-booking_class',
 'B-fromloc.city_name',
 'I-flight_days',
 'B-restriction_code',
 'B-stoploc.airport_name',
 'I-depart_date.today_relative',
 'I-stoploc.city_name',
 'B-compartment',
 'B-time',
 'B-arrive_time.period_mod',
 'I-depart_time.time_relative',
 'I-time',
 'B-depart_date.day_number',
 'I-flight_number',
 'B-aircraft_code',
 'B-month_name',
 'B-city_name',
 'I-fromloc.airport_name',
 'I-class_type',
 'B-fromloc.airport_code',
 'B-depart_time.start_time',
 'B-or',
 'B-round_trip',
 'B-depart_date.today_relative',
 'I-meal_description',
 'I-arrive_time.time_relative',
 'O',
 'I-mod',
 'B-mod',
 'B-toloc.state_name',
 'B-flight_number',
 'B-arrive_time.time_relative',
 'B-state_code',
 'B-arrive_date.date_relative',
 'B-return_date.today_relative',
 'B-flight_mod',
 'I-state_name',
 'B-meal',
 'B-arrive_date.month_name',
 'I-fromloc.state_name',
 'B-arrive_time.period_of_day',
 'B-period_of_day',
 'B-airport_code',
 'B-class_type',
 'B-toloc.city_name',
 'B-flight_time']

                        )
                    ),
                }
            ),
            supervised_keys=None,
            homepage=None,
            citation=_CITATION,
        )

    def _split_generators(self, dl_manager):
        """Returns SplitGenerators."""

        downloaded_files = dl_manager.download_and_extract(self.config.data_files)

        return [
            datasets.SplitGenerator(name=datasets.Split.TRAIN, gen_kwargs={"filepath": downloaded_files["train"]}),
            datasets.SplitGenerator(name=datasets.Split.VALIDATION, gen_kwargs={"filepath": downloaded_files["dev"]}),
            datasets.SplitGenerator(name=datasets.Split.TEST, gen_kwargs={"filepath": downloaded_files["test"]}),
        ]

    def _generate_examples(self, filepath):
        logger.info("⏳ Generating examples from = %s", filepath)
        with open(filepath[0], encoding="utf-8") as f:
            guid = 0
            tokens = []
            chunk_tags = []
            for line in f:
                if line.startswith("-DOCSTART-") or line == "" or line == "\n":
                    if tokens:
                        yield guid, {
                            "id": str(guid),
                            "tokens": tokens,
                            "chunk_tags": chunk_tags,
                        }
                        guid += 1
                        tokens = []
                        chunk_tags = []
                else:
                    # conll2003 tokens are space separated
                    splits = line.split(" ")
                    tokens.append(splits[0])
                    chunk_tags.append(splits[1])
            # last example
            yield guid, {
                "id": str(guid),
                "tokens": tokens,
                "chunk_tags": chunk_tags,
            }

