#for model in qwant/fralbert-base camembert/camembert-base camembert/camembert-base-wikipedia-4gb camembert/camembert-base-ccnet camembert/camembert-large camembert/camembert-base-oscar-4gb camembert/camembert-base-ccnet-4gb 
for model in bert-base-multilingual-cased distilbert-base-multilingual-cased
#xlm-roberta-large xlm-roberta-base bert-base-multilingual-cased distilbert-base-multilingual-cased 
#flaubert/flaubert_base_uncased flaubert/flaubert_base_cased flaubert/flaubert_small_cased flaubert/flaubert_large_cased
do
	log=`echo $model | perl -pe 's,/,_,g'`
	time python3 _tagging-hf-MEDIA-COST.py --model $model --epoch 10 >& log.$log
done
