# coding=utf-8
# Copyright 2020 HuggingFace Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lint as: python3
"""Introduction to the CoNLL-2003 Shared Task: Language-Independent Named Entity Recognition"""

import datasets


logger = datasets.logging.get_logger(__name__)


_CITATION = """\
@inproceedings{tjong-kim-sang-de-meulder-2003-introduction,
    title = "Introduction to the {C}o{NLL}-2003 Shared Task: Language-Independent Named Entity Recognition",
    author = "Tjong Kim Sang, Erik F.  and
      De Meulder, Fien",
    booktitle = "Proceedings of the Seventh Conference on Natural Language Learning at {HLT}-{NAACL} 2003",
    year = "2003",
    url = "https://www.aclweb.org/anthology/W03-0419",
    pages = "142--147",
}
"""

_DESCRIPTION = """\
The shared task of CoNLL-2003 concerns language-independent named entity recognition. We will concentrate on
four types of named entities: persons, locations, organizations and names of miscellaneous entities that do
not belong to the previous three groups.

The CoNLL-2003 shared task data files contain four columns separated by a single space. Each word has been put on
a separate line and there is an empty line after each sentence. The first item on each line is a word, the second
a part-of-speech (POS) tag, the third a syntactic chunk tag and the fourth the named entity tag. The chunk tags
and the named entity tags have the format I-TYPE which means that the word is inside a phrase of type TYPE. Only
if two phrases of the same type immediately follow each other, the first word of the second phrase will have tag
B-TYPE to show that it starts a new phrase. A word with tag O is not part of a phrase. Note the dataset uses IOB2
tagging scheme, whereas the original dataset uses IOB1.

For more details see https://www.clips.uantwerpen.be/conll2003/ner/ and https://www.aclweb.org/anthology/W03-0419
"""

_URL = None
_TRAINING_FILE = "train.txt"
_DEV_FILE = "dev.txt"
_TEST_FILE = "test.txt"


class MediaConfig(datasets.BuilderConfig):
    """BuilderConfig for MEDIA"""

    def __init__(self, **kwargs):
        """BuilderConfig for MEDIA.

        Args:
          **kwargs: keyword arguments forwarded to super.
        """
        super(MediaConfig, self).__init__(**kwargs)


class Media(datasets.GeneratorBasedBuilder):
    """Conll2003 dataset."""

    BUILDER_CONFIGS = [
        MediaConfig(name="media", version=datasets.Version("0.0.0"), description="Media dataset"),
    ]

    def _info(self):
        return datasets.DatasetInfo(
            description=_DESCRIPTION,
            features=datasets.Features(
                {
                    "id": datasets.Value("string"),
                    "tokens": datasets.Sequence(datasets.Value("string")),
                    "chunk_tags": datasets.Sequence(
                        datasets.features.ClassLabel(
                            names=[
                                'B-chambre-equipement',
 'I-nombre-hotel',
 'B-connectAttr',
 'B-localisation-region',
 'B-nombreNonDigit-hotel',
 'B-unknown',
 'I-temps-plageTps',
 'B-temps-plageTps',
 'B-comparatif',
 'I-temps-jourFerie',
 'B-chambre-type',
 'I-hotel-parking',
 'I-chambre-standing',
 'B-nombre-chambre',
 'B-sejour-nbPersonne',
 'I-nombre-reservation',
 'B-paiement-monnaie',
 'B-nom-hotel',
 'B-sejour-nbEnfant',
 'I-sejour-nbEnfant',
 'I-localisation-arrondissement',
 'I-hotel-services',
 'I-sejour-nbCouple',
 'B-hotel-services',
 'I-localisation-region',
 'I-localisation-codePostal',
 'I-chambre-equipement',
 'B-localisation-lieuRelatif',
 'B-sejour-nbNuit',
 'I-rang',
 'B-sejour-nbCouple',
 'B-temps-jour-mois',
 'B-nombre',
 'I-paiement-montantQualitatif',
 'I-objetBD',
 'I-comparatif-paiement',
 'B-chambre-fumeur',
 'B-rang-temps',
 'B-rang',
 'B-sejour-nbLitBebe',
 'B-temps-jour-semaine',
 'I-rang-temps',
 'B-rang-hotel',
 'I-localisation-pays',
 'B-localisation-cardinal',
 'B-evenement',
 'B-hotel-etat-complet',
 'I-comparatif-temps',
 'B-localisation-quartier',
 'B-rang-reservation',
 'I-nom-client',
 'I-rang-hotel',
 'I-localisation-lieuRelatif',
 'B-nom',
 'I-temps-jour-semaine',
 'I-localisation-cardinal',
 'B-lienRef-coRef',
 'B-objet',
 'B-temps-annee',
 'I-localisation-ville',
 'B-temps-mois',
 'I-lienRef-coRef',
 'I-paiement-methodeDePaiement',
 'I-reponse',
 'I-nombre-chambre',
 'B-hotel-marque',
 'B-temps-date',
 'B-hotel-etoile',
 'B-nombre-temps',
 'B-nombre-hotel',
 'B-chambre-voisine',
 'I-nombreNonDigit-hotel',
 'B-localisation-ville',
 'I-nombreNonDigit',
 'B-lienRef-coDom',
 'I-connectAttr',
 'B-localisation-rue',
 'I-rang-reservation',
 'I-temps-unite',
 'I-hotel-marque',
 'I-command-tache',
 'B-temps-unite',
 'B-localisation-arrondissement',
 'I-connectProp',
 'I-unknown',
 'I-temps-annee',
 'B-command-tache',
 'B-reponse',
 'B-command-dial',
 'I-hotel-etat-complet',
 'B-temps-axeTps',
 'I-sejour-nbLitBebe',
 'I-localisation-departement',
 'I-nom-hotel',
 'B-paiement-montantQualitatif',
 'B-nombre-restaurant',
 'I-sejour-nbPersonne',
 'B-nombre-reservation',
 'B-localisation-distanceRelative',
 'I-paiement-montant-entier',
 'B-nombreNonDigit',
 'I-temps-date',
 'B-paiement-methodeDePaiement',
 'B-temps-heure',
 'I-localisation-rue',
 'B-comparatif-temps',
 'I-nombre-restaurant',
 'I-temps-jour-mois',
 'I-comparatif',
 'B-nombre-chambre-disponible',
 'B-localisation-departement',
 'B-sejour-nbAdulte',
 'I-personne-nomDeFamille',
 'B-temps-plageRelative',
 'I-temps-plageRelative',
 'B-personne-nomDeFamille',
 'B-connectProp',
 'I-command-dial',
 'O',
 'I-hotel-etoile',
 'I-chambre-type',
 'I-localisation-distanceRelative',
 'B-temps-jourFerie',
 'I-sejour-nbNuit',
 'B-chambre-standing',
 'B-localisation-pays',
 'B-localisation-codePostal',
 'I-nom',
 'I-nombre-temps',
 'I-lienRef-coDom',
 'I-localisation-quartier',
 'I-temps-mois',
 'B-objetBD',
 'B-comparatif-paiement',
 'B-hotel-parking',
 'I-objet',
 'B-lienRef-elsEns',
 'I-chambre-fumeur',
 'I-lienRef-elsEns',
 'I-temps-heure',
 'B-hotel-etat',
 'I-sejour-nbAdulte',
 'I-evenement',
 'I-nombre',
 'I-nombre-chambre-disponible',
 'I-temps-axeTps',
 'B-paiement-montant-entier',
 'B-nom-client',
 'I-chambre-voisine', 'B-localisation', 'I-localisation'
                                  ]

                        )
                    ),
                }
            ),
            supervised_keys=None,
            homepage=None,
            citation=_CITATION,
        )

    def _split_generators(self, dl_manager):
        """Returns SplitGenerators."""

        downloaded_files = dl_manager.download_and_extract(self.config.data_files)

        return [
            datasets.SplitGenerator(name=datasets.Split.TRAIN, gen_kwargs={"filepath": downloaded_files["train"]}),
            datasets.SplitGenerator(name=datasets.Split.VALIDATION, gen_kwargs={"filepath": downloaded_files["dev"]}),
            datasets.SplitGenerator(name=datasets.Split.TEST, gen_kwargs={"filepath": downloaded_files["test"]}),
        ]

    def _generate_examples(self, filepath):
        logger.info("⏳ Generating examples from = %s", filepath)

        with open(filepath[0], encoding="utf-8") as f:
            guid = 0
            tokens = []
            chunk_tags = []
            for line in f:
                if line.startswith("-DOCSTART-") or line == "" or line == "\n":
                    if tokens:
                        yield guid, {
                            "id": str(guid),
                            "tokens": tokens,
                            "chunk_tags": chunk_tags,
                        }
                        guid += 1
                        tokens = []
                        chunk_tags = []
                else:
                    # conll2003 tokens are space separated
                    splits = line.split(" ")
                    tokens.append(splits[0])
                    chunk_tags.append(splits[1])
            # last example
            yield guid, {
                "id": str(guid),
                "tokens": tokens,
                "chunk_tags": chunk_tags,
            }

